import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:software_enginering/general/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:software_enginering/screens/confirmation.dart';
import 'package:software_enginering/screens/home.dart';
import 'dart:async';

import 'package:software_enginering/screens/registration.dart';
import 'dart:ui';
import 'package:flutter/widgets.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  final TextEditingController email = new TextEditingController();
  final TextEditingController password = new TextEditingController();
  final _databaseReference = Firestore.instance;

  final FirebaseAuth _auth = FirebaseAuth.instance;

  Future<FirebaseUser> _handleSignIn(String email, String password) async {
    final FirebaseUser user = (await _auth.signInWithEmailAndPassword(
            email: email, password: password))
        .user;
    return user;
  }

  _checkUser(FirebaseUser user) {
    try {
      _databaseReference
          .collection("Users")
          .document(user.uid)
          .get()
          .then((value) {
        if (value.data["seller"] == false) {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => Home(user, false)));
        } else {
          if (value.data["confirmed"] == false) {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => Confirmation(user, true)));
          } else {
            Navigator.of(context).pushReplacement(
                MaterialPageRoute(builder: (context) => Home(user, true)));
          }
        }
      });
    } catch (error) {
      print(error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                logo(),
                promotionText(),
                userInfo(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget logo() {
    return Icon(
      Icons.local_grocery_store,
      color: colorOrange,
      size: 150,
    );
  }

  Widget promotionText() {
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Text(
        'BAZZAR is a programme that is uniting all your favorite places,gives you an opportunity to seize the opportunities of their interesting offers and special campaigns',
        textAlign: TextAlign.center,
        style: TextStyle(
            fontSize: 24,
            color: Colors.black,
            fontFamily: 'DancingScript',
            fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget userInfo() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              child: TextFormField(
                key: ValueKey("email-field"),
                controller: email,
                textInputAction: TextInputAction.next,
                decoration: InputDecoration(
                  labelText: 'Email',
                  suffixIcon: Icon(Icons.person),
                  labelStyle: TextStyle(color: colorOrange),
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorOrange),
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Form(
              child: TextFormField(
                key: ValueKey("password-field"),
                controller: password,
                textInputAction: TextInputAction.done,
                obscureText: true,
                decoration: InputDecoration(
                  labelText: 'Password',
                  hasFloatingPlaceholder: true,
                  suffixIcon: Icon(Icons.vpn_key),
                  labelStyle: TextStyle(color: colorOrange),
                  border: OutlineInputBorder(),
                  focusedBorder: OutlineInputBorder(
                    borderSide: BorderSide(color: colorOrange),
                  ),
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.centerRight,
            child: FlatButton(
              child: Text(
                'Forgot password?',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
              onPressed: () {},
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              height: 50,
              width: MediaQuery.of(context).size.width,
              child: RaisedButton(
                key: ValueKey("signIn-button"),
                child: Text(
                  'Log in',
                  style: TextStyle(
                      color: Colors.white,
                      fontSize: 18,
                      fontWeight: FontWeight.bold),
                ),
                color: colorOrange,
                elevation: 0,
                onPressed: () {
                  _handleSignIn(email.text, password.text).then((user) {
//                    Navigator.of(context).pushReplacement(
//                        MaterialPageRoute(builder: (context) => Home(user)));
                    _checkUser(user);
                  }).catchError((error) async {
                    await showDialog(
                      context: context,
                      builder: (BuildContext context) {
                        return CupertinoAlertDialog(
                          title: new Text("Error"),
                          content: new Text("${error.toString()}"),
                          actions: <Widget>[
                            new FlatButton(
                              child: new Text("Close"),
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                            ),
                          ],
                        );
                      },
                    );
                  });
                },
              ),
            ),
          ),
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text(
                    'Don\'t have an account?',
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                  FlatButton(
                    key: ValueKey("signUp-from-login"),
                    child: Text(
                      'Register',
                      style: TextStyle(
                          color: colorOrange, fontWeight: FontWeight.bold),
                    ),
                    onPressed: () {
                      Navigator.of(context).push(MaterialPageRoute(
                          builder: (context) => Registration()));
                    },
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

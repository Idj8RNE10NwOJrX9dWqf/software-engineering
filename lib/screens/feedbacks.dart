import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class Feedbacks extends StatefulWidget {
  final String productID;

  Feedbacks(this.productID);

  @override
  _FeedbacksState createState() => _FeedbacksState();
}

class _FeedbacksState extends State<Feedbacks> {
  final _databaseReference = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Feedbacks',
          style: TextStyle(color: Colors.black),
        ),
        brightness: Brightness.light,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: StreamBuilder(
          stream: _databaseReference
              .collection("Feedbacks")
              .document(widget.productID)
              .collection("feedbacks")
              .snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text('Error: ${snapshot.error}'),
              );
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(
                  child: Text("Waiting..."),
                );
              default:
                return ListView(
                    children: snapshot.data.documents.map<Widget>((document) {
                  return Card(
                    child: ListTile(
                      leading: StreamBuilder(
                          stream: _databaseReference
                              .collection("Users")
                              .document(document["sender"])
                              .snapshots(),
                          builder: (context, snapshot) {
                            return CircleAvatar(
                              backgroundImage:
                                  NetworkImage(snapshot.data["profile_image"]),
                            );
                          }),
                      title: StreamBuilder(
                          stream: _databaseReference
                              .collection("Users")
                              .document(document["sender"])
                              .snapshots(),
                          builder: (context, snapshot) {
                            return Text(snapshot.data["name"] +
                                " " +
                                snapshot.data["surname"]);
                          }),
                      subtitle: Text(document["message"]),
                    ),
                  );
                }).toList());
            }
          }),
    );
  }
}

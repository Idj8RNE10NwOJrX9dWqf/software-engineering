import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class Chats extends StatefulWidget {
  final FirebaseUser user;

  Chats(this.user);

  @override
  _ChatsState createState() => _ChatsState();
}

class _ChatsState extends State<Chats> {
  final _databaseReference = Firestore.instance;

  String room(String sender, String receiver) {
    return sender.compareTo(receiver) > 1
        ? sender + receiver
        : receiver + sender;
  }

  String partnerID(String userOne, String userTwo) {
    return userOne != widget.user.uid.toString() ? userOne : userTwo;
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
        stream: _databaseReference.collection("Chats").snapshots(),
        builder: (BuildContext context, AsyncSnapshot snapshot) {
          if (snapshot.hasError)
            return Center(
                child: Text("There is some errors: ${snapshot.error}"));
          switch (snapshot.connectionState) {
            case ConnectionState.waiting:
              return Center(
                child: Text("Waiting..."),
              );
            case ConnectionState.none:
              return Center(
                child: Text("Waiting..."),
              );
            default:
              return Padding(
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                child: ListView(
                    children: snapshot.data.documents.map<Widget>((document) {
                  if (document.documentID
                      .toString()
                      .contains(widget.user.uid)) {
                    return StreamBuilder(
                        stream: _databaseReference
                            .collection("Users")
                            .document(partnerID(
                                document["receiver"], document["sender"]))
                            .snapshots(),
                        builder: (context, snapshot) {
                          return ListTile(
                            leading: CircleAvatar(
                              radius: 25,
                              backgroundColor: Colors.blue,
                              backgroundImage:
                                  NetworkImage(snapshot.data["profile_image"]),
                            ),
                            title: Text("${snapshot.data["name"]}"),
                            subtitle: Text("${document["text"]}"),
                          );
                        });
                  }
                  return null;
                }).toList()),
              );
          }
        },
      ),
    );
  }
}

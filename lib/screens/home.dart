import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:software_enginering/general/colors.dart';
import 'package:software_enginering/screens/MyProducts.dart';
import 'package:software_enginering/screens/chats.dart';
import 'package:software_enginering/screens/explore.dart';
import 'package:software_enginering/screens/login.dart';
import 'package:software_enginering/screens/product_add.dart';
import 'package:software_enginering/screens/profile_edit.dart';

class Home extends StatefulWidget {
  final FirebaseUser user;
  final bool isSeller;
  Home(this.user, this.isSeller);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  final _databaseReference = Firestore.instance;
  final _auth = FirebaseAuth.instance;

  static FirebaseUser user;

  @override
  void initState() {
    user = widget.user;
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: ValueKey("home-page"),
      appBar: AppBar(
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 0,
        title: Text('BAZZAR',
            style: TextStyle(
                color: Colors.black, fontFamily: "DancingScript-Bold")),
      ),
      body: widget.isSeller ? MyProducts(user) : Explore(user),
      endDrawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            UserAccountsDrawerHeader(
              accountName: StreamBuilder(
                stream: _databaseReference
                    .collection('Users')
                    .document(widget.user.uid)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Text("Loading...");
                  }
                  return Text(
                    "${snapshot.data["name"]} ${snapshot.data["surname"]}",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  );
                },
              ),
              accountEmail: StreamBuilder(
                stream: _databaseReference
                    .collection('Users')
                    .document(widget.user.uid)
                    .snapshots(),
                builder: (context, snapshot) {
                  if (!snapshot.hasData) {
                    return Text("Loading...");
                  }
                  return Text(
                    "${snapshot.data["email"]}",
                    style: TextStyle(fontSize: 16),
                  );
                },
              ),
              currentAccountPicture: StreamBuilder(
                  stream: _databaseReference
                      .collection('Users')
                      .document(widget.user.uid)
                      .snapshots(),
                  builder: (context, snapshot) {
                    return CircleAvatar(
                      backgroundColor: colorOrange,
                      backgroundImage:
                          NetworkImage(snapshot.data["profile_image"]),
                    );
                  }),
              decoration: BoxDecoration(color: colorOrange),
            ),
            InkWell(
              child: ListTile(
                leading: Icon(Icons.edit),
                title: Text(
                  'Edit Personal Data',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (context) => ProfileEdit(widget.user.uid)));
              },
            ),
            InkWell(
              child: ListTile(
                leading: Icon(Icons.settings),
                title: Text(
                  'Settings',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              onTap: () {},
            ),
            InkWell(
              child: ListTile(
                leading: Icon(Icons.info),
                title: Text(
                  'About',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              onTap: () {},
            ),
            InkWell(
              child: ListTile(
                leading: Icon(Icons.close),
                title: Text(
                  'Log out',
                  style: TextStyle(fontSize: 16),
                ),
              ),
              onTap: () async {
                await _auth.signOut();
                Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => Login()));
              },
            ),
          ],
        ),
      ),
      floatingActionButton: widget.isSeller
          ? FloatingActionButton(
              key: ValueKey("floating-add-button"),
              backgroundColor: colorOrange,
              child: Icon(Icons.add),
              onPressed: () {
                Navigator.of(context).push(
                    MaterialPageRoute(builder: (context) => AddProduct(user)));
              },
            )
          : null,
    );
  }
}

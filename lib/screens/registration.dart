import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:software_enginering/general/colors.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:software_enginering/screens/confirmation.dart';
import 'package:software_enginering/screens/home.dart';

class Registration extends StatefulWidget {
  @override
  _RegistrationState createState() => _RegistrationState();
}

class _RegistrationState extends State<Registration> {
  final TextEditingController name = new TextEditingController();
  final TextEditingController surname = new TextEditingController();
  final TextEditingController email = new TextEditingController();
  final TextEditingController password = new TextEditingController();
  final TextEditingController confirmPassword = new TextEditingController();
  final TextEditingController phoneNumber = new TextEditingController();

  final _auth = FirebaseAuth.instance;
  final databaseReference = Firestore.instance;

  var _isSeller = false;

  Future<FirebaseUser> _handleRegistration(
      String email, String password) async {
    final FirebaseUser user = (await _auth.createUserWithEmailAndPassword(
            email: email, password: password))
        .user;
    return user;
  }

  void _addUser(FirebaseUser user) async {
    try {
      await databaseReference.collection("Users").document(user.uid).setData({
        "name": name.text,
        "surname": surname.text,
        "email": email.text,
        "password": password.text,
        "phone_number": phoneNumber.text,
        "profile_image": "",
        "seller": _isSeller,
        "confirmed": _isSeller ? false : true
      }).then((value) async {
        if (_isSeller) {
          await databaseReference
              .collection("Confirmations")
              .document(user.uid)
              .setData({"code": UniqueKey().toString()}).then((value) {
            Navigator.of(context).pushReplacement(MaterialPageRoute(
                builder: (context) => Confirmation(user, _isSeller)));
          });
        } else {
          Navigator.of(context).pushReplacement(
              MaterialPageRoute(builder: (context) => Home(user, _isSeller)));
        }
      });
    } catch (error) {
      print("ERROR: " + error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        iconTheme: IconThemeData(color: Colors.black),
        brightness: Brightness.light,
        title: Text(
          'Registation',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: Center(
          child: SingleChildScrollView(
            physics: const AlwaysScrollableScrollPhysics(),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                userInformation(),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget profileImage() {
    return Stack(
      children: <Widget>[
        Container(
          height: 100,
          width: 100,
          child: CircleAvatar(
            backgroundImage: NetworkImage(""),
          ),
        ),
        Container(
          height: 110,
          width: 110,
          child: IconButton(
            alignment: Alignment.bottomRight,
            icon: Icon(Icons.photo_camera),
            color: colorOrange,
            iconSize: 40,
            onPressed: () {
              print("get image");
            },
          ),
        ),
      ],
    );
  }

  Widget userInformation() {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Expanded(child: textField("Name", null, name, "reg-name-field")),
            Expanded(
                child:
                    textField("Surname", null, surname, "reg-surname-field")),
          ],
        ),
        textField("Email", Icon(Icons.person), email, "reg-email-field"),
        textField(
            "Password", Icon(Icons.vpn_key), password, "reg-password-field"),
        textField("Confirm Password", Icon(Icons.vpn_key), confirmPassword,
            "reg-confirm-field"),
        textField(
            "Phone Number", Icon(Icons.phone), phoneNumber, "reg-number-field"),
        checkSellerOrNot(),
        confirmationButton(),
        goToLogin(),
      ],
    );
  }

  Widget textField(String hint, Icon icon, TextEditingController controller,
      String valueKey) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        child: TextFormField(
          key: ValueKey(valueKey),
          controller: controller,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            labelText: hint,
            suffixIcon: icon,
            labelStyle: TextStyle(color: colorOrange),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: colorOrange),
            ),
          ),
        ),
      ),
    );
  }

  Widget confirmationButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          key: ValueKey("reg-reg-button"),
          child: Text(
            'Registration',
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          color: colorOrange,
          elevation: 0,
          onPressed: () {
            _handleRegistration(email.text, password.text).then((user) {
              user.sendEmailVerification();
              _addUser(user);
            }).catchError((error) async {
              await showDialog(
                context: context,
                builder: (BuildContext context) {
                  // return object of type Dialog
                  return CupertinoAlertDialog(
                    title: new Text("Error"),
                    content: new Text("${error.toString()}"),
                    actions: <Widget>[
                      new FlatButton(
                        child: new Text("Close"),
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                      ),
                    ],
                  );
                },
              );
            });
          },
        ),
      ),
    );
  }

  Widget goToLogin() {
    return Align(
      alignment: Alignment.center,
      child: Padding(
        padding: const EdgeInsets.all(30.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Text(
              'Do you have an account?',
              style: TextStyle(fontWeight: FontWeight.bold),
            ),
            Padding(
              padding: const EdgeInsets.all(.0),
              child: FlatButton(
                child: Text(
                  'Log in',
                  style: TextStyle(
                      color: colorOrange, fontWeight: FontWeight.bold),
                ),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            )
          ],
        ),
      ),
    );
  }

  Widget checkSellerOrNot() {
    return Column(
      children: <Widget>[
        Card(
          elevation: 1,
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
          child: Container(
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Text(
                  "Are you seller?",
                  style: TextStyle(fontSize: 16),
                ),
                CupertinoSwitch(
                  key: ValueKey("reg-switch"),
                  value: _isSeller,
                  onChanged: (value) {
                    setState(() {
                      _isSeller = !_isSeller;
                    });
                  },
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}

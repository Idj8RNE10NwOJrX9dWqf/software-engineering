import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ProfileInfo extends StatefulWidget {
  final String userUID;
  ProfileInfo(this.userUID);

  @override
  _ProfileInfoState createState() => _ProfileInfoState();
}

class _ProfileInfoState extends State<ProfileInfo> {
  final _databaseReference = Firestore.instance;

  @override
  void initState() {
    print(widget.userUID);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        title: Text(
          'Profile info',
          style: TextStyle(color: Colors.black),
        ),
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: StreamBuilder(
          stream: _databaseReference
              .collection("Users")
              .document(widget.userUID)
              .snapshots(),
          builder: (context, snapshot) {
            return Stack(
              children: <Widget>[
                Column(
                  children: <Widget>[
                    Container(
                      height: 260,
                      width: MediaQuery.of(context).size.width,
                      color: Colors.grey.shade200,
                      child: GestureDetector(
                        child: Image(
                          image: NetworkImage(snapshot.data["profile_image"]),
                        ),
                        onTap: () {
                          print("Change the cover");
                        },
                      ),
                    ),
                    Expanded(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                          children: <Widget>[
                            Container(
                                width: MediaQuery.of(context).size.width,
                                height: 90),
                            Align(
                              child: Text(
                                snapshot.data["name"] +
                                    " " +
                                    snapshot.data["surname"],
                                style: TextStyle(
                                    fontWeight: FontWeight.bold, fontSize: 24),
                              ),
                              alignment: Alignment.center,
                            ),
                            Texts(snapshot.data["location"] ?? "",
                                Icons.location_city),
                            Texts(snapshot.data["phone_number"] ?? "",
                                Icons.phone),
                            Texts(snapshot.data["email"] ?? "", Icons.mail),
                            Texts(snapshot.data["short_bio"] ?? "", Icons.info),
                          ],
                        ),
                      ),
                    )
                  ],
                ),
                Positioned(
                  left: MediaQuery.of(context).size.width / 2 - 80,
                  top: 180,
                  child: CircleAvatar(
                    backgroundColor: Colors.white,
                    radius: 80,
                    backgroundImage: NetworkImage(
                      "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Yin_yang.svg/1200px-Yin_yang.svg.png",
                    ),
                  ),
                )
              ],
            );
          }),
    );
  }

  Widget Texts(String title, IconData textIcon) {
    return ListTile(
      title: Text(title, style: TextStyle(fontSize: 18)),
      leading: Icon(textIcon),
    );
  }
}

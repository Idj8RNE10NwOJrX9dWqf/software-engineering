import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:software_enginering/screens/profile_info.dart';
import 'product_info.dart';

class MyProducts extends StatefulWidget {
  final FirebaseUser user;

  MyProducts(this.user);
  @override
  _MyProductsState createState() => _MyProductsState();
}

class _MyProductsState extends State<MyProducts> {
  final _databaseReference = Firestore.instance;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: StreamBuilder(
          stream: _databaseReference
              .collection("Products")
              .where("user", isEqualTo: widget.user.uid)
              .snapshots(),
          builder: (context, snapshot) {
            if (snapshot.hasError) {
              return Center(
                child: Text('Error: ${snapshot.error}'),
              );
            }
            switch (snapshot.connectionState) {
              case ConnectionState.waiting:
                return Center(
                  child: Text("Waiting..."),
                );
              default:
                return ListView(
                    children: snapshot.data.documents.map<Widget>((document) {
                  return GestureDetector(
                    onTap: () {
                      Navigator.of(context).push(
                        MaterialPageRoute(
                          builder: (context) => ProductInfo(
                              document["product_photo"],
                              document["title"],
                              document["description"],
                              widget.user,
                              document["user"],
                              document.documentID),
                        ),
                      );
                    },
                    child: Card(
                      elevation: 1,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(16.0),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.all(Radius.circular(16.0)),
                          image: DecorationImage(
                            fit: BoxFit.cover,
                            image: NetworkImage(document["product_photo"]),
                          ),
                        ),
                        height: 300,
                        child: Stack(
                          children: <Widget>[
                            Align(
                              alignment: Alignment.bottomCenter,
                              child: Container(
                                decoration: BoxDecoration(
                                    color: Colors.black.withOpacity(0.7),
                                    borderRadius: BorderRadius.all(
                                        Radius.circular(10.0))),
                                height: 70,
                                child: Align(
                                  alignment: Alignment.center,
                                  child: ListTile(
                                    title: Text(
                                      document["title"],
                                      style: TextStyle(
                                          fontSize: 18,
                                          color: Colors.white,
                                          fontWeight: FontWeight.bold),
                                    ),
                                    subtitle: Text(
                                      document["description"],
                                      overflow: TextOverflow.ellipsis,
                                      style: TextStyle(
                                          fontSize: 16,
                                          color: Colors.white,
                                          fontWeight: FontWeight.w500),
                                    ),
                                    trailing: Icon(
                                      Icons.offline_bolt,
                                      color: document["sold"] == false
                                          ? Colors.green
                                          : Colors.red,
                                    ),
                                    leading: StreamBuilder(
                                      stream: _databaseReference
                                          .collection("Users")
                                          .document(document["user"])
                                          .snapshots(),
                                      builder: (context, snapshot) {
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).push(
                                                MaterialPageRoute(
                                                    builder: (context) =>
                                                        ProfileInfo(
                                                            document["user"])));
                                          },
                                          child: CircleAvatar(
                                            radius: 25.0,
                                            backgroundColor: Colors.transparent,
                                            backgroundImage: NetworkImage(
                                                snapshot.data["profile_image"]),
                                          ),
                                        );
                                      },
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                }).toList());
            }
          }),
    );
  }
}

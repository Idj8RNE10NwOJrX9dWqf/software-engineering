import 'package:test/test.dart';
import 'package:flutter_driver/flutter_driver.dart';

void main() {
  group('Test App', () {
    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    //LOGIN TEST

    final emailField = find.byValueKey("email-field");
    final passwordField = find.byValueKey("password-field");
    final signInButton = find.byValueKey("signIn-button");
    final alert = find.byType("CupertinoAlertDialog");
    final registerFromLogin = find.byValueKey("signUp-from-login");
//
//    test(
//      'Login fails with wrong email and password and shows dialog',
//      () async {
//        await driver.tap(emailField);
//        await driver.enterText("ali.almasli.98@gmail.com");
//        await driver.tap(passwordField);
//        await driver.enterText("wrongPassword");
//        await driver.tap(signInButton);
//        await driver.waitFor(alert);
//        assert(alert != null);
//        await driver.waitUntilNoTransientCallbacks();
//      },
//    );
//
//    test('Login pass with corret email and password', () async {
//      await driver.tap(emailField);
//      await driver.enterText("ali.almasli.98@gmail.com");
//      await driver.tap(passwordField);
//      await driver.enterText("123456");
//      await driver.tap(signInButton);
//    });
//
//    test(
//      'Login fails with wrong email and password and shows dialog',
//      () async {
//        await driver.tap(emailField);
//        await driver.enterText("gulgun.ferziyeva@gmail.com");
//        await driver.tap(passwordField);
//        await driver.enterText("wrongPassword");
//        await driver.tap(signInButton);
//        await driver.waitFor(alert);
//        assert(alert != null);
//        await driver.waitUntilNoTransientCallbacks();
//      },
//    );

    //REGISTRATION TEST

//    final regNameField = find.byValueKey("reg-name-field");
//    final regSurnameField = find.byValueKey("reg-surname-field");
//    final regEmailField = find.byValueKey("reg-email-field");
//    final regPasswordField = find.byValueKey("reg-password-field");
//    final regConfirmField = find.byValueKey("reg-confirm-field");
//    final regNumberField = find.byValueKey("reg-number-field");
//    final regSwitch = find.byValueKey("reg-switch");
//    final regButton = find.byValueKey("reg-reg-button");
//
//    test('Resgistration test', () async {
//      await driver.tap(registerFromLogin);
//      await driver.tap(regNameField);
//      await driver.enterText("Ali");
//      await driver.tap(regSurnameField);
//      await driver.enterText("Almasli");
//      await driver.tap(regEmailField);
//      await driver.enterText("ali.almasli.98@gmail.com");
//      await driver.tap(regPasswordField);
//      await driver.enterText("123456");
//      await driver.tap(regConfirmField);
//      await driver.enterText("123456");
//      await driver.tap(regNumberField);
//      await driver.enterText("+994507825544");
//      await driver.tap(regSwitch);
//      await driver.tap(regButton);
//      await driver.waitFor(alert);
//    });

    final homePage = find.byValueKey("home-page");
    final floatingButton = find.byValueKey("floating-add-button");
    final floatingAddImage = find.byValueKey("floating-add-image");
    final imageGallery = find.byType("ImagePicker");
    final titleField = find.byValueKey("type-field");

    test('Test Create Post', () async {
      await driver.tap(emailField);
      await driver.enterText("ali.almasli.98@gmail.com");
      await driver.tap(passwordField);
      await driver.enterText("123456");
      await driver.tap(signInButton);
      await driver.waitFor(homePage);
      await driver.tap(floatingButton);
      await driver.tap(floatingAddImage);
      await driver.waitUntilNoTransientCallbacks();
      await driver.waitFor(imageGallery);
      assert(imageGallery != null);
      await driver.waitUntilNoTransientCallbacks();
      await driver.tap(titleField);
      await driver.enterText("Some text");
    });
  });
}

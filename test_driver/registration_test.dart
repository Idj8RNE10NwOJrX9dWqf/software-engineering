import 'package:test/test.dart';
import 'package:flutter_driver/flutter_driver.dart';

void main() {
  group('Registration test', () async {
    final registerFromLogin = find.byValueKey("signUp-from-login");

    FlutterDriver driver;

    setUpAll(() async {
      driver = await FlutterDriver.connect();
    });

    tearDownAll(() async {
      if (driver != null) {
        driver.close();
      }
    });

    test('Resgistration test', () async {
      await driver.tap(registerFromLogin);
    });
  });
}

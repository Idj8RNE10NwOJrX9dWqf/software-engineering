import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class ProfileEdit extends StatefulWidget {
  final String userUID;

  ProfileEdit(this.userUID);

  @override
  _ProfileEditState createState() => _ProfileEditState();
}

class _ProfileEditState extends State<ProfileEdit> {
  final _databaseReference = Firestore.instance;

  final name = TextEditingController();
  final surname = TextEditingController();
  final location = TextEditingController();
  final shortBio = TextEditingController();
  final phone = TextEditingController();
  final email = TextEditingController();

  @override
  void initState() {
    _getUserData();
    super.initState();
  }

  _getUserData() async {
    try {
      await _databaseReference
          .collection("Users")
          .document(widget.userUID)
          .get()
          .then((value) {
        name.text = value["name"];
        surname.text = value["surname"];
        phone.text = value["phone_number"];
        email.text = value["email"];
        location.text = value["location"] ?? "";
        shortBio.text = value["short_bio"] ?? "";
      });
    } catch (error) {
      print(error.toString());
    }
  }

  _updateUserInfo() async {
    try {
      await _databaseReference
          .collection("Users")
          .document(widget.userUID)
          .updateData({
        "name": name.text,
        "surname": surname.text,
        "phone_number": phone.text,
        "email": email.text,
        "short_bio": shortBio.text
      });
    } catch (error) {
      print(error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0,
        title: Text(
          "Edit profile",
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.save_alt),
            onPressed: () {
              _updateUserInfo();
            },
          )
        ],
      ),
      body: StreamBuilder(
          stream: _databaseReference
              .collection("Users")
              .document(widget.userUID)
              .snapshots(),
          builder: (context, snapshot) {
            return Padding(
              padding: const EdgeInsets.all(8.0),
              child: SingleChildScrollView(
                physics: AlwaysScrollableScrollPhysics(),
                child: Column(
                  children: <Widget>[
                    profilePic(),
                    textFields(
                        "Name", 1, TextInputType.text, Icons.person, name),
                    textFields("Surname", 1, TextInputType.text,
                        Icons.person_pin, surname),
                    textFields("Location", 1, TextInputType.text,
                        Icons.person_pin_circle, location),
                    textFields("Short Bio", 3, TextInputType.text, Icons.pages,
                        shortBio),
                    textFields(
                        "Phone", 1, TextInputType.phone, Icons.phone, phone),
                    textFields("Email", 1, TextInputType.emailAddress,
                        Icons.mail, email)
                  ],
                ),
              ),
            );
          }),
    );
  }

  Widget profilePic() {
    return Center(
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Stack(
          children: <Widget>[
            Container(
              height: 100,
              width: 100,
              child: CircleAvatar(
                backgroundImage: NetworkImage(
                  "https://upload.wikimedia.org/wikipedia/commons/thumb/1/17/Yin_yang.svg/1200px-Yin_yang.svg.png",
                ),
              ),
            ),
            Container(
              height: 110,
              width: 110,
              child: IconButton(
                alignment: Alignment.bottomRight,
                icon: Icon(Icons.camera_alt),
                iconSize: 40,
                color: Colors.orange,
                onPressed: () {},
              ),
            ),
          ],
        ),
      ),
    );
  }

  Widget textFields(String labelText, int line, TextInputType typeofbox,
      IconData textIcon, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 4.0),
      child: TextField(
        controller: controller,
        keyboardType: typeofbox,
        obscureText: false,
        decoration: InputDecoration(
          prefixIcon: Icon(textIcon),
          border: OutlineInputBorder(),
          labelText: labelText,
        ),
        maxLines: line,
      ),
    );
  }
}

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:software_enginering/general/colors.dart';
import 'package:image_picker/image_picker.dart';
import 'dart:io';

class AddProduct extends StatefulWidget {
  final FirebaseUser user;

  AddProduct(this.user);

  @override
  _AddProductState createState() => _AddProductState();
}

class _AddProductState extends State<AddProduct> {
  final _databaseReference = Firestore.instance;
  final _storageReference = FirebaseStorage.instance;

  final description = TextEditingController();
  final price = TextEditingController();
  final title = TextEditingController();

  File _image;

  Future getImage() async {
    var image = await ImagePicker.pickImage(source: ImageSource.gallery);
    setState(() {
      _image = image;
    });
  }

  Future<String> uploadPic() async {
    StorageReference reference =
        _storageReference.ref().child("Products/${UniqueKey()}");
    StorageUploadTask uploadTask = reference.putFile(_image);
    String location = await (await uploadTask.onComplete).ref.getDownloadURL();
    return location;
  }

  void _addProduct(String url) async {
    try {
      await _databaseReference.collection("Products").document().setData({
        "description": description.text,
        "product_photo": url,
        "sold": false,
        "title": title.text,
        "user": widget.user.uid,
      });
      Navigator.of(context).pop();
    } catch (error) {
      print("ERROR: " + error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        brightness: Brightness.light,
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
        title: Text('Sell Product',
            style: TextStyle(
                color: Colors.black, fontFamily: "DancingScript-Bold")),
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          children: <Widget>[
            photo(),
            textField(
                "Title for product", Icon(Icons.title), title, "title-field"),
            textField("Description of product", Icon(Icons.description),
                description, "desc-field"),
            textField('Price', Icon(Icons.attach_money), price, "price-field"),
            confirmationButton()
          ],
        ),
      ),
    );
  }

  Widget photo() {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: MediaQuery.of(context).size.height * 0.3,
      child: Stack(
        children: <Widget>[
          Center(
            child: _image != null
                ? Image.file(_image, fit: BoxFit.cover)
                : Image(image: NetworkImage("")),
          ),
          Align(
            alignment: Alignment.bottomRight,
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: FloatingActionButton(
                key: ValueKey("floating-add-image"),
                child: Icon(Icons.camera_enhance),
                onPressed: () {
                  getImage();
                },
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget textField(String hint, Icon icon, TextEditingController controller,
      String valueKey) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        child: TextFormField(
          key: ValueKey(valueKey),
          controller: controller,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            labelText: hint,
            suffixIcon: icon,
            labelStyle: TextStyle(color: colorOrange),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: colorOrange),
            ),
          ),
        ),
      ),
    );
  }

  Widget confirmationButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 50,
        width: MediaQuery.of(context).size.width,
        child: RaisedButton(
          child: Text(
            'Sell',
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          color: colorOrange,
          elevation: 0,
          onPressed: () {
            uploadPic().then((url) {
              _addProduct(url);
            }).catchError((onError) {
              print("ERROR: $onError");
            });
          },
        ),
      ),
    );
  }
}

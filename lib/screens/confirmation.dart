import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:software_enginering/general/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:software_enginering/screens/home.dart';

class Confirmation extends StatefulWidget {
  final FirebaseUser user;
  final bool isSeller;

  Confirmation(this.user, this.isSeller);

  @override
  _ConfirmationState createState() => _ConfirmationState();
}

class _ConfirmationState extends State<Confirmation> {
  final code = TextEditingController();
  final _databaseReference = Firestore.instance;

  _confirmUser(FirebaseUser user, BuildContext context) async {
    try {
      _databaseReference
          .collection("Confirmations")
          .document(user.uid)
          .get()
          .then((value) async {
        if (value.data["code"] == code.text) {
          _updateUser(user);
        } else {
          await showDialog(
            context: context,
            builder: (BuildContext context) {
              return CupertinoAlertDialog(
                title: new Text("Error"),
                content: new Text("Code is wrong"),
                actions: <Widget>[
                  new FlatButton(
                    child: new Text("Close"),
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              );
            },
          );
        }
      });
    } catch (error) {
      print(error.toString());
    }
  }

  _updateUser(FirebaseUser user) async {
    try {
      await _databaseReference
          .collection("Users")
          .document(user.uid)
          .updateData({"confirmed": true});
      await _databaseReference
          .collection("Confirmations")
          .document(user.uid)
          .delete();
      Navigator.of(context).pushReplacement(
          MaterialPageRoute(builder: (context) => Home(user, widget.isSeller)));
    } catch (error) {
      print(error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.white,
        title: Text(
          'Confirmation',
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(16.0),
              child: Text(
                'If you are seller please contact with our company and '
                'provide us with more information about your local '
                'store. It will be given you a confirmation code to submit your store is legal',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                textAlign: TextAlign.center,
              ),
            ),
            ListTile(
              leading: Icon(Icons.call),
              title: Text(
                '+99450782554',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Icon(Icons.call),
              title: Text(
                '+994515858655',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Icon(Icons.email),
              title: Text(
                'ali.almasli.98@gmail.com',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            ListTile(
              leading: Icon(Icons.email),
              title: Text(
                'gulgun.ferziyeva@gmail.com',
                style: TextStyle(fontWeight: FontWeight.bold),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: textField(
                  'Enter confirmaiton code', Icon(Icons.vpn_key), code),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: confirmationButton(),
            ),
          ],
        ),
      ),
    );
  }

  Widget textField(String hint, Icon icon, TextEditingController controller) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Form(
        child: TextFormField(
          style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
          controller: controller,
          textInputAction: TextInputAction.next,
          decoration: InputDecoration(
            labelText: hint,
            suffixIcon: icon,
            labelStyle: TextStyle(color: colorOrange),
            border: OutlineInputBorder(
              borderSide: BorderSide(color: colorOrange),
            ),
            focusedBorder: OutlineInputBorder(
              borderSide: BorderSide(color: colorOrange),
            ),
          ),
        ),
      ),
    );
  }

  Widget confirmationButton() {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Container(
        height: 40,
        width: MediaQuery.of(context).size.width * 0.4,
        child: RaisedButton(
          child: Text(
            'Confirm',
            style: TextStyle(
                color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
          ),
          color: colorOrange,
          elevation: 0,
          onPressed: () {
            _confirmUser(widget.user, context);
          },
        ),
      ),
    );
  }
}

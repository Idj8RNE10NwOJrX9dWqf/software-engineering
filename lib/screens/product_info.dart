import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:software_enginering/general/colors.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:software_enginering/screens/feedbacks.dart';

class ProductInfo extends StatefulWidget {
  final FirebaseUser user;
  final String productPhoto;
  final String productName;
  final String productDescription;
  final String receiver;
  final String productID;

  ProductInfo(this.productPhoto, this.productName, this.productDescription,
      this.user, this.receiver, this.productID);

  @override
  _ProductInfoState createState() => _ProductInfoState();
}

class _ProductInfoState extends State<ProductInfo> {
  final _textFieldController = TextEditingController();
  final _databaseReference = Firestore.instance;

  _displayDialog(BuildContext context, String sender, String receiver) async {
    return await showDialog(
      context: context,
      builder: (context) {
        return CupertinoAlertDialog(
          title: Text('Feedback'),
          content: Card(
            elevation: 0.5,
            child: CupertinoTextField(
              controller: _textFieldController,
              placeholder: "Type...",
            ),
          ),
          actions: <Widget>[
            new CupertinoDialogAction(
              child: new Text('Cancel'),
              onPressed: () {
                Navigator.of(context).pop();
              },
            ),
            new CupertinoDialogAction(
              child: new Text('Ok'),
              onPressed: () {
                _addMessage(sender, receiver);
              },
            )
          ],
        );
      },
    );
  }

  _addMessage(String sender, String receiver) async {
    try {
      await _databaseReference
          .collection("Feedbacks")
          .document(widget.productID)
          .collection("feedbacks")
          .document()
          .setData({
        "message": _textFieldController.text,
        "sender": sender,
      });
      Navigator.of(context).pop();
    } catch (error) {
      print("ERROR: " + error.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Brightness.light,
        elevation: 0,
        title: Text(
          'Product info',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(color: Colors.black),
      ),
      body: SingleChildScrollView(
        physics: AlwaysScrollableScrollPhysics(),
        child: Column(
          children: <Widget>[
            image(),
            ListTile(
              title: Text(
                widget.productName,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18),
              ),
              trailing: Text(
                "1000 AZN",
                style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    color: colorOrange),
              ),
            ),
            ListTile(
              title: Text(widget.productDescription),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: ListTile(
                  onTap: () {
                    Navigator.of(context).push(MaterialPageRoute(
                        builder: (context) => Feedbacks(widget.productID)));
                  },
                  leading: Icon(Icons.feedback),
                  trailing: Icon(Icons.arrow_forward_ios),
                  title: Text(
                    "Feedbacks",
                    style: TextStyle(fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.chat),
        onPressed: () {
          _displayDialog(context, widget.user.uid, widget.receiver);
        },
      ),
    );
  }

  Widget image() {
    return Stack(
      children: <Widget>[
        Container(
          height: MediaQuery.of(context).size.height * 0.3,
          decoration: BoxDecoration(
            image: DecorationImage(
              fit: BoxFit.contain,
              image: NetworkImage(widget.productPhoto),
            ),
          ),
        ),
      ],
    );
  }
}

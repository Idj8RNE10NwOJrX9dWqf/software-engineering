import 'package:flutter_driver/driver_extension.dart';
import 'package:software_enginering/main.dart' as app;

void main() {
  enableFlutterDriverExtension();
  app.main();
}
